# Getting started

Assuming Docker is installed, run the following in a terminal:

```docker-compose up``` from the directory containing the ```docker-compose.yml``` file.

All being well, visit http://localhost:1111 in your browser.
