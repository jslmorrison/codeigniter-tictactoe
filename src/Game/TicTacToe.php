<?php

namespace Game;

use Game\GamePlayer;
use Ramsey\Uuid\Uuid;

class TicTacToe
{
    protected $id;
    protected $player1;
    protected $player2;
    protected $moves;
    protected $usedGridRefs;
    const MAXMOVES = 9;

    private function __construct()
    {
        $this->id = Uuid::uuid4()->toString();
        $this->moves = [];
        $this->usedGridRefs = [];
    }

    public static function start(GamePlayer $player1, GamePlayer $player2)
    {
        $ticTacToe = new TicTacToe();
        $ticTacToe->player1 = $player1;
        $ticTacToe->player2 = $player2;

        return $ticTacToe;
    }

    public function id()
    {
        return $this->id;
    }

    public function player1()
    {
        return $this->player1;
    }

    public function player2()
    {
        return $this->player2;
    }

    public function moves()
    {
        return $this->moves;
    }

    public function makeMove(GamePlayer $player, $gridRef)
    {
        if (in_array($gridRef, $this->usedGridRefs)) {
            throw new \Exception('invalid move, grid ref already used');
        }

        $this->usedGridRefs[] = $gridRef;
        $this->moves[] = [$player, $gridRef];
    }

    public function maxMoves()
    {
        return self::MAXMOVES;
    }
}
