<?php

namespace Game;

use Game\GamePlayer;

class Player implements GamePlayer
{
    protected $name;

    private function __construct()
    {
    }

    public static function named($name)
    {
        $player = new Player();
        $player->name = $name;

        return $player;
    }

    public function name()
    {
        return $this->name;
    }
}
