<?php

namespace spec\Game;

use Game\Player;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Game\GamePlayer;

class PlayerSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Player::class);
    }

    function it_should_implement_game_player_interface(GamePlayer $gamePlayer)
    {
        $this->shouldImplement(GamePlayer::class);
    }

    function let()
    {
        $this->beConstructedThrough('named', ['John']);
    }

    function it_should_have_a_name()
    {
        $this->name()->shouldReturn('John');
    }
}
