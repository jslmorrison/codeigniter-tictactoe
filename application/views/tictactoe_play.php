<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php include(__DIR__.'/partials/header.inc.php'); ?>
<div class="col-xs-12 col-md-8">
<table class="table table-bordered">
<?php $x = 1; for($row = 1; $row < 4; $row++): ?>
<tr>
<?php for($col = 1; $col < 4; $col++): ?>
<td class="square" id="sq_<?php echo $x; ?>" data-gridRef="<?php echo $x; ?>"></td>
<?php $x++; ?>
<?php endfor ?>
</tr>
<?php endfor ?>
</table>
<input type="hidden" name="gameId" id="gameId" value="<?php echo $gameId; ?>"/>
</div>
<div class="col-xs-4 hidden-xs hidden-sm">
<p>Last 5 winners</p>
<?php if (count($winners) === 0): ?>
<p>There are no winners yet.</p>
<?php else: ?>
<table class="table table-striped">
<?php foreach($winners as $winner): ?>
<tr>
<td><?php echo $winner->player; ?></td>
<td><?php echo date('r', $winner->game_end_time); ?></td>
</tr>
<?php endforeach; ?>
</table>
<a href="/tictactoe/winners">More winners</a>
<?php endif; ?>
</div>

<?php include(__DIR__.'/partials/footer.inc.php'); ?>