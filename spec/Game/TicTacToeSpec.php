<?php

namespace spec\Game;

use Game\TicTacToe;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Game\GamePlayer;

class TicTacToeSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(TicTacToe::class);
    }

    function let(GamePlayer $player1, GamePlayer $player2)
    {
        $this->beConstructedThrough('start', [$player1, $player2]);
    }

    function it_should_have_an_id()
    {
        $this->id()->shouldNotBeNull();
    }

    function it_should_have_a_player_1()
    {
        $this->player1()->shouldBeAnInstanceOf(GamePlayer::class);
    }

    function it_should_have_a_player_2()
    {
        $this->player2()->shouldBeAnInstanceOf(GamePlayer::class);
    }

    function it_should_have_a_collection_of_moves()
    {
        $this->moves()->shouldBeArray();
    }

    function it_should_be_able_to_add_move_to_collection(GamePlayer $player)
    {
        $this->moves()->shouldHaveCount(0);
        $this->makeMove($player, 5);
        $this->moves()->shouldHaveCount(1);
    }

    function it_should_throw_exception_if_grid_ref_already_used(GamePlayer $player1, GamePlayer $player2)
    {
        $this->moves()->shouldHaveCount(0);
        $this->makeMove($player1, 5);
        $this->shouldThrow('\Exception')->duringMakeMove($player1, 5);
        $this->shouldThrow('\Exception')->duringMakeMove($player2, 5);
        $this->moves()->shouldHaveCount(1);
        $this->shouldNotThrow('\Exception')->duringMakeMove($player1, 1);
        $this->moves()->shouldHaveCount(2);
    }

    function it_should_have_a_max_number_of_moves()
    {
        $this->maxMoves()->shouldReturn(9);
    }
}
