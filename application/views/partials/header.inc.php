<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style type="text/css">
    td.square {
        height: 3em;
        margin: 1em;
        text-align: center;
        font-size: 3em;
    }
    </style>
    <title>Welcome to CodeIgniter</title>
</head>
<body>

<div class="container-fluid">
	<h1>Welcome to TicTacToe CodeIgniter!</h1>

	<div id="body" class="row">