<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
	<!-- <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p> -->
</div>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"
    integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    crossorigin="anonymous">
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
    integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
    crossorigin="anonymous">
</script>
<script type="text/javascript">
    $(document).ready(function() {

        var gameId = $('#gameId').val();
        var movesMade = 0;
        var gameOver = false;
        var player1Moves = [];
        var player2Moves = [];
        var winningMoves = [
            ['1','2','3'],
            ['4','5','6'],
            ['7','8','9'],
            ['1','4','7'],
            ['2','5','8'],
            ['3','6','9'],
            ['1','5','9'],
            ['3','5','7']
        ];

        function updateBoard(player, gridRef) {

            // console.log(player + ' clicked grid ref ' + gridRef);
            if (player == 'player1') {
                playerToken = 'X';
                sessionStorage.currentPlayer = 'player2';
            }
            if (player == 'player2') {
                playerToken = 'O';
                sessionStorage.currentPlayer = 'player1';
            }

            if ($('#sq_' + gridRef).text().length == 0) {

                $('#sq_' + gridRef).text(playerToken);

                var jqxhr = $.ajax({
                    method: "POST",
                    url: "/tictactoe/move",
                    data: {
                        game: $('#gameId').val(),
                        player: player,
                        gridRef: gridRef 
                    }
                })
                .done(function(data) {
                    movesMade++;
                    if (movesMade > 8) {
                        confirm('Game over. Its a draw!');
                        gameOver = true;
                    }
                    // console.log(data);
                    isMoveTheWinningMove(player, gridRef);
                })
                .fail(function() {
                    console.log('failed to update board');
                })
                .always(function() {
                    console.log('moves made = ' + movesMade);
                });
            }
        }

        function isMoveTheWinningMove(player, gridRef)
        {
            var moves;
            switch(player) {
                case 'player1':
                player1Moves.push(gridRef);
                moves = player1Moves.sort();
                // console.log(moves);
                break;
                case 'player2':
                player2Moves.push(gridRef);
                moves = player2Moves.sort();
                // console.log(moves);
                break;
            }

            winningMoves.forEach(function(match){
                if (moves.length > 2 && match.toString() == moves.toString()) {
                    gameOver = true;
                    winnerIs(player);
                    alert('Congratulations ' + player + ', you have won.');
                    return;
                }
            });
        }

        function winnerIs(player) {
            $.ajax({
                method: 'POST',
                url: '/tictactoe/winner',
                data: { gameId: gameId, player: player }
            })
            .done(function() {
                console.log('winner sent to controller');
            })
            .fail(function() {
                console.log('error sending winner to controller')
            });
        }

        sessionStorage.currentPlayer = 'player1';
        $('.square').click(function() {
            if (gameOver == true) {
                if (confirm('Game over already.Play again?')) {
                    window.location.assign('/tictactoe/play');
                }
                return;
            }
            gridRef = $(this).attr('data-gridRef');
            updateBoard(sessionStorage.currentPlayer, gridRef);
        });
    });
</script>
</body>
</html>