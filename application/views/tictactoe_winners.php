<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php include(__DIR__.'/partials/header.inc.php'); ?>
<div class="col-xs-12">
<a href="/tictactoe/play">Play game</a>
<?php if(!isset($winners) || count($winners) === 0): ?>
<p>There are currently no winners</p>
<? else: ?>
<table class="table table-striped">
<tr>
<td>Game</td>
<td>Player</td>
<td>Date</td>
</tr>
<?php foreach($winners as $winner): ?>
<tr>
<td><?php echo $winner->id; ?></td>
<td><?php echo $winner->player; ?></td>
<td><?php echo date('r', $winner->game_end_time); ?></td>
</tr>
<?php endforeach; ?>
</table>
<?php endif; ?>
</div>

<?php include(__DIR__.'/partials/footer.inc.php'); ?>
