<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php include(__DIR__.'/partials/header.inc.php'); ?>
<div class="col-xs-12">
<p>Please enter player names.</p>
<?php echo form_open(base_url().'tictactoe/play'); ?>
<div class="form-group">
<?php echo form_label('Player 1 name', 'player1'); ?>
<?php echo form_input(
    [
        'name' => 'player1',
        'class' => 'form-control',
        'placeholder' => 'Player1 name',
        'required' => true
    ]
); ?>
</div>
<div class="form-group">
<?php echo form_label('Player 2 name.', 'player2'); ?>
<?php echo form_input(
    [
        'name' => 'player2',
        'class' => 'form-control',
        'placeholder' => 'Player2 name',
        'required' => true
    ]
); ?>
</div>
<?php echo form_submit('submit', 'Begin game'); ?>
<?php echo form_close(); ?>
</div>
<?php include(__DIR__.'/partials/footer.inc.php'); ?>