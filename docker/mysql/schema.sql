CREATE TABLE `games` (
`id` varchar(36) NOT NULL,
`player1` varchar(36) NOT NULL,
`player2` varchar(36) NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `moves` (
`player` varchar(45) NOT NULL,
`gridref` tinyint(1) unsigned zerofill NOT NULL,
`game_id` varchar(36) NOT NULL,
KEY `moves_ibfk_1` (`game_id`),
CONSTRAINT `moves_ibfk_1` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `winners` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`game_id` varchar(36) NOT NULL,
`player` varchar(36) NOT NULL,
`game_end_time` varchar(45) NOT NULL,
PRIMARY KEY (`id`),
KEY `fk_new_table_1_idx` (`game_id`),
CONSTRAINT `fk_new_table_1` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
