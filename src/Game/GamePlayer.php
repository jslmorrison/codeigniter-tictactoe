<?php

namespace Game;

interface GamePlayer
{
    public static function named($name);
    public function name();
}
