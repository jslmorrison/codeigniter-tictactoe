<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// todo -all database queries should ideally be in a repository class
class TicTacToeController extends CI_Controller
{
    public function index()
    {
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->view('tictactoe_start');
    }

    public function play()
    {
        $this->load->helper('url');
        $this->load->database();

        if ($this->input->method() !== 'post') {
            header('Location: '.base_url().'tictactoe');
        }

        $game = Game\TicTacToe::start(
            Game\Player::named($this->input->post()['player1']),
            Game\Player::named($this->input->post()['player2'])
        );

        if ($game instanceof Game\TicTacToe) {
            $data = [
                'id' => $game->id(),
                'player1' => $game->player1()->name(),
                'player2' => $game->player2()->name()
            ];
            $this->db->insert('games', $data);
        }

        $query = $this->db->select('*')
            ->from('winners')
            ->limit(5)
            ->order_by('game_end_time', 'desc')
            ->get();
        $winners = $query->result();

        $this->load->view(
            'tictactoe_play',
            [
                'gameId' => $game->id(),
                'winners' => $winners
            ]
        );
    }

    public function makeMove()
    {
        if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
            exit('No direct script access allowed');
        }
        $this->load->database();
        $query = $this->db->select('*')
            ->from('games')
            ->join('moves as moves', 'moves.game_id = games.id', 'left')
            ->where('id', $this->input->post()['game'])
            ->get();
        $game = $query->result();

        if (!empty($game) && (count($game) < Game\TicTacToe::MAXMOVES)) {
            $move = [
                'game_id' => $game[0]->id,
                'player' => $this->input->post()['player'],
                'gridref' => $this->input->post()['gridRef']
            ];
            $this->db->insert('moves', $move);
            // echo json_encode(['OK - move persisted']);
        }

        // header('Content-Type: application/json');
    }

    public function winner()
    {
        if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
            exit('No direct script access allowed');
        }
        $this->load->database();
        $winner = [
            'game_id' => $this->input->post()['gameId'],
            'player' => $this->input->post()['player'],
            'game_end_time' => time()
        ];
        $this->db->insert('winners', $winner);
        // echo 'OK - winner persisted';
    }

    public function winners()
    {
        $this->load->database();
        $query = $this->db->select('*')
            ->from('winners')
            ->order_by('game_end_time', 'desc')
            ->get();
        $data['winners'] = $query->result();
        $this->load->view('tictactoe_winners', $data);
    }
 }